# -*- coding: utf-8 -*-

'''
Dependency: py4j
How to install: pip install py4j

Gets stems of a word in Python 3 using py4j. In the output 

1: stands for noun, 
2: stands for adjective, 
3: stands for verb, 
4:stands for adverb

'''


from py4j.java_gateway import JavaGateway
from py4j.java_gateway import java_import
    
gateway = JavaGateway.launch_gateway(classpath="hindi-wn-simple.jar")
#import the Java class
java_import(gateway.jvm,'sivareddy.in.WordnetToolsSimple')

#read the word
#word = input("Input a word: ")
#word = word.strip().decode("utf-8", "ignore")
	
word = "हँसे"
#call the static methods
gateway.jvm.WordnetToolsSimple.initialize()
roots = gateway.jvm.WordnetToolsSimple.getRoot(word)

#form a list in case of multiple roots
if roots.find(";") != -1:
    roots = roots.split(";")#form a list
    roots = roots[:-1] #remove the last empty element
    
print(roots)